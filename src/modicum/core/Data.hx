package modicum.core;

import modicum.lib.Observable;

class Data<T> extends Observable {
	public var data(default,null): T;

	public function new(?d:T) {
		super((count, observer) -> {
			observer != null ? observer(this, data) : null;
		});
		this.data = d;
	}

	public function trigger(?_) {
		notifyObservers(this, data);
	}

}
