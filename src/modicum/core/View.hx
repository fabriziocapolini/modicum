package modicum.core;

import modicum.core.Data;

using StringTools;
using modicum.lib.DomTools;

typedef ViewProps = {
	?dom: DomElement,
	?markup: String,
	?plug: String,
	?before: String,
	?classes: Array<String>,
	?onupdate: (View)->Void,

	?status: Data<Dynamic>,
	?statuspath: (View, Dynamic)->Dynamic,
	?onstatus: (View, Dynamic)->Void,
	?childrenstatus: (View, Dynamic)->Dynamic,

	?data: Data<Dynamic>,
	?datapath: (View, Dynamic)->Dynamic,
	?ondata: (View, Dynamic)->Void,
	?childrendata: (View, Dynamic)->Dynamic,

	?theme: Data<Dynamic>,
	?themepath: (View, Dynamic)->Dynamic,
	?ontheme: (View, Dynamic)->Void,
	?childrentheme: (View, Dynamic)->Dynamic,
}

class View {
	public var parent(default,null): View;
	public var children(default,null): Array<View>;
	public var dom(default,null): DomElement;
	
	public function new(parent:View, props:ViewProps, ?cb:(View)->Void, ?cloneOf:View) {
		this.parent = parent;
		this.props = props;
		this.cb = cb;
		this.cloneOf = cloneOf;
		this.children = [];
		dom = makeDom();
		init();
		updateSelf();
		link();
		cb != null ? cb(this) : null;
		props.status != null ? props.status.addObserver(statusObserver) : null;
		props.data != null ? props.data.addObserver(dataObserver) : null;
		props.theme != null ? props.theme.addObserver(themeObserver) : null;
	}

	public function dispose() {
		for (child in children) {
			child.dispose();
		}
		unlink();
	}

	public function update() {
		updateSelf();
		for (child in children) {
			child.update();
		}
	}

	public function updateSelf() {
		props.onupdate != null ? props.onupdate(this) : null;
	}

	public function set(aka:String, text:String) {
		var node = nodes.get(aka);
		if (node != null) {
			_set(node, text);
		} else {
			trace('unknown "$aka" value');
		}
	}

	function _set(node:ViewNode, text:String) {
		text == null ? text = '' : null;
		do {
			if (node.p[node.i] != text) {
				node.p[node.i] = text;
				node.t != null ? node.t.domSetValue(node.p.join('')) : null;
			}
		} while ((node = node.next) != null);
	}

	public inline function getElement(aka:String): DomElement {
		return cast nodes.get(aka).e;
	}

	public function statusObserver(_, d:Dynamic) {
		setStatus(d);
	}

	public function setStatus(d:Dynamic) {
		props.statuspath != null ? d = props.statuspath(this, d) : null;
		if (d != null) {
			autostatus ? onauto(d, 'status.') : null;
			props.onstatus != null ? props.onstatus(this, d) : null;
			props.childrenstatus != null ? d = props.childrenstatus(this, d) : null;
			for (child in children) {
				if (child.props.status == null) {
					child.setStatus(d);
				}
			}
		}
	}

	public function dataObserver(_, d:Dynamic) {
		setData(d);
	}

	public function setData(d:Dynamic, ?useDatapath:Bool=true) {
		useDatapath && props.datapath != null ? d = props.datapath(this, d) : null;
		if (Std.is(d, Array)) {
			setArray(cast d);
		} else if (d != null) {
			rangeData = null;
			dom.domSetClass('hidden', false);
			autodata ? onauto(d, 'data.') : null;
			props.ondata != null ? props.ondata(this, d) : null;
			props.childrendata != null ? d = props.childrendata(this, d) : null;
			for (child in children) {
				if (child.props.data == null) {
					child.setData(d);
				}
			}
		} else {
			dom.domSetClass('hidden', true);
			clearClones(); 
		}
	}

	function onauto(d:Dynamic, context:String) {
		function tryPath(node:ViewNode, path:String, offset:Int): Bool {
			var parts = path.split('.');
			var v = d;
			for (i in offset...parts.length) {
				var part = parts[i];
				if (v != null && Reflect.hasField(v, part)) {
					v = Reflect.getProperty(v, part);
				} else {
					return false;
				}
			}
			_set(node, v);
			return true;
		}
		for (name in nodes.keys()) {
			if (name.startsWith(context)) {
				// var path = name.split('.');
				// var v = d;
				// for (i in 1...path.length) {
				// 	v != null ? v = Reflect.getProperty(v, path[i]) : null;
				// }
				// set(name, v);
				var node = nodes.get(name);
				var paths = name.split('|');
				var offset = 1;
				for (path in paths) {
					if (tryPath(node, path, offset)) {
						break;
					}
					offset = 0;
				}
			}
		}
	}

	public function setDataRange(start:Int, ?end:Int) {
		rangeStart = start;
		rangeEnd = end;
		rangeData != null ? setArray(rangeData) : null;
	}

	public function themeObserver(_, d:Dynamic) {
		setTheme(d);
	}

	public function setTheme(d:Dynamic) {
		props.themepath != null ? d = props.themepath(this, d) : null;
		if (d != null) {
			autotheme ? onauto(d, 'theme.') : null;
			props.ontheme != null ? props.ontheme(this, d) : null;
			props.childrentheme != null ? d = props.childrentheme(this, d) : null;
			for (child in children) {
				if (child.props.theme == null) {
					child.setTheme(d);
				}
			}
		}
	}

	// =========================================================================
	// private
	// =========================================================================
	static inline var EXP_START = '[[';
	static inline var EXP_END = ']]';
	var props: ViewProps;
	var cb: (View)->Void;
	var nodes: Map<String, ViewNode>;
	var autostatus = false;
	var autodata = false;
	var autotheme = false;

	function link() {
		if (parent != null) {
			var plug = props.plug != null ? props.plug : 'default';
			var pdom = parent.getElement(plug);
			// pdom == null ? trace('unknown slot "$plug"') : null;
			pdom == null ? pdom = parent.getElement('default') : null;
			if (cloneOf != null) {
				props.dom != null ? null : pdom.domAppend(dom, cloneOf.dom);
			} else {
				parent.children.push(this);
				// props.dom != null ? null : pdom.domAppend(dom);
				if (props.dom == null) {
					var ref = parent.nodes.get(props.before);
					ref != null ? pdom = ref.e.domParent() : null;
					pdom != null ? pdom.domAppend(dom, ref != null ? ref.e : null) : null;
				}
			}
		}
	}

	function unlink() {
		if (parent != null) {
			cloneOf != null ? null : parent.children.remove(this);
			dom.remove();
		}
	}

	function init() {
		nodes = new Map();
		collectNodes(dom);
		nodes.exists('default') ? null : nodes.set('default', {e:dom});
		nodes.set('root', {e:dom});
		if (props.classes != null) {
			for (klass in props.classes) {
				dom.domSetClass(klass, true);
			}
		}
		if (props.ondata != null) {
			dom.domSetClass('hidden', true);
		}
	}

	function makeDom() {
		var ret: DomElement;
		if (props.dom != null) {
			ret = props.dom;
		} else if (props.markup != null) {
			// var e = root.doc.domCreateElement('div');
			var e = parent.dom.domOwnerDocument().domCreateElement('div');
			e.innerHTML = ~/\n\s+/g.replace(props.markup, '\n');
			ret = e.domFirstElementChild();
		} else {
			// ret = root.doc.domCreateElement('div');
			ret = parent.dom.domOwnerDocument().domCreateElement('div');
		}
		return ret;
	}

	function collectNodes(e:DomElement) {
		var aka = e.getAttribute('aka');
		if (aka != null) {
			e.domSetAttr('aka', null);
			nodes.set(aka, {e:e});
		}
		e.domMapChildren((n) -> {
			if (Std.is(n, DomElement)) {
				collectNodes(cast n);
			} else if (Std.is(n, DomText)) {
				collectTexts(cast n);
			}
			return true;
		});
	}

	function collectTexts(t:DomText) {
		var s = t.domGetValue();
		var i = 0, i1 = 0, i2 = 0, name = null;
		var parts = null, names = null;
		while ((i1 = s.indexOf(View.EXP_START, i)) >= 0
			&& (i2 = s.indexOf(View.EXP_END, i1)) > i1) {
			parts == null ? parts = new Array<String>() : null;
			if (i1 > i) {
				parts.push(s.substring(i, i1));
			}
			name = s.substring(i1 + View.EXP_START.length, i2).trim();
			if (name.length > 0) {
				names == null ? names = new Map<String, Int>() : null;
				names.set(name, parts.length);
				var next = nodes.get(name);
				nodes.set(name, {t:t, p:parts, i:parts.length, next:next});
				parts.push('');
				!autostatus && name.startsWith('status.') ? autostatus = true : null;
				!autodata && name.startsWith('data.') ? autodata = true : null;
				!autotheme && name.startsWith('theme.') ? autotheme = true : null;
			}
			i = i2 + View.EXP_END.length;
		}
		if (parts != null && parts.length > 0) {
			if (i < s.length) {
				parts.push(s.substr(i));
			}
			t.domSetValue(parts.join(''));
		}
	}

	// =========================================================================
	// replication
	// =========================================================================
	var rangeStart = 0;
	var rangeEnd = null;
	var rangeData: Array<Dynamic>;
	var cloneOf: View;
	var clones: Array<View>;

	/*
	Clones are Views whose cloneOf is set and they're linked into
	the DOM but not added to the View tree.
	Depending on array length:
	- if zero, only the original View exists and it's hidden
	- if one, only the original View exists, populated and visible
	- if more than one, the original View is the last element of the sequence
	*/
	function setArray(v:Array<Dynamic>) {
		rangeData = v;
		if (rangeStart != 0 || rangeEnd != null) {
			v = rangeEnd != null
				? v.slice(rangeStart, rangeEnd)
				: v.slice(rangeStart);
		}
		var count:Int = cast Math.max(v.length - 1, 0);
		clones != null ? null : clones = [];
		for (i in 0...count) {
			if (i >= clones.length) {
				clones.push(new View(parent, props, cb, this));
			}
			clones[i].setData(v[i], false);
		}
		clearClones(count);
		setData(v.length > 0 ? v[v.length - 1] : null, false);
	}

	function clearClones(count=0) {
		if (clones != null) {
			while (clones.length > count) {
				clones.pop().unlink();
			}
		}
	}

}

typedef ViewNode = {
	?e: DomElement,
	?t: DomText,
	?p: Array<String>,
	?i: Int,
	?next: ViewNode,
}
