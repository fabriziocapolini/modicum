package modicum.core;

import modicum.core.Data;
import modicum.core.View.ViewProps;

using modicum.lib.DomTools;

typedef PageProps = {
	> ViewProps,
	?styles: Array<ViewProps>,
}

class Page extends View {
	public var doc(default,null): DomDocument;
	public var head(default,null): View;
	public var body(default,null): View;
	
	public function new(props:PageProps, ?cb:Page->Void) {
		props.dom == null ? props.dom = DomTools.getDefaultDoc().domRoot() : null;
		this.doc = props.dom.domOwnerDocument();
		super(null, props, (p) -> {
			new View(p, {dom: doc.domHead()}, (head) -> {
				new View(p, {dom: doc.domBody()}, (body) -> {
					this.head = head;
					this.body = body;
					nodes.set('head', {e:head.dom});
					nodes.set('body', {e:body.dom});
					nodes.set('default', {e:body.dom});
					new View(head, {markup: '<meta name="viewport" '
						+ 'content="width=device-width, initial-scale=1">'
					});
					addStyles(props.styles);
					cb != null ? cb(cast p) : null;
					update();
				});
			});
		});
	}

	function addStyles(styles:Array<ViewProps>) {
		if (styles != null) {
			for (style in styles) {
				new View(head, style);
			}					
		}
	}

}
