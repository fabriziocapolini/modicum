package modicum.lib;

class Safe {
	public static function str(v:Dynamic, def=null, pre=null, post=null): String {
		v == null ? v = def : null;
		v != null && pre != null ? v = '${pre}${v}' : null;
		v != null && post != null ? v = '${v}${post}' : null;
		return (v != null ? cast v : '');
	}
}
