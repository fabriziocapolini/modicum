package modicum.lib;

typedef Sender = Dynamic;
typedef Observer = (Sender, Dynamic) -> Void;

// =============================================================================
// Observable
// =============================================================================

class Observable extends DoubleLinkedItem {
	var _observers:Array<Observer>;
	var _cb:Int->Observer->Void;

	public function new(?cb:Int->Observer->Void) {
		super();
		this._cb = cb;
	}

	public inline function setCallback(cb:Int->Observer->Void) {
		this._cb = cb;
	}

	public function addObserver(o:Observer) {
		if (_observers == null) {
			_observers = [];
		}
		if (_observers.indexOf(o) < 0) {
			_observers.push(o);
			if (_cb != null) {
				_cb(_observers.length, o);
			}
		}
	}

	public function removeObserver(o:Observer):Bool {
		var ret = false;
		if (_observers != null && _observers.remove(o)) {
			if (_observers.length < 1) {
				_observers = null;
			}
			ret = true;
		}
		if (_cb != null) {
			_cb(_observers != null ? _observers.length : 0, null);
		}
		return ret;
	}

	public function clearObservers() {
		if (_observers != null) {
			while (_observers.length > 0) {
				_observers.pop();
			}
		}
		if (_cb != null) {
			_cb(_observers != null ? _observers.length : 0, null);
		}
	}

	public function hasObservers():Bool {
		return (_observers != null ? _observers.length > 0 : false);
	}

	public function notifyObservers(sender:Sender, arg:Dynamic) {
		sender = (sender != null ? sender : this);
		if (_observers != null) {
			//			for (o in _observers) {
			//				o(sender, arg);
			//			}
			var n = _observers.length - 1;
			for (i in 0..._observers.length) {
				_observers[n - i](sender, arg);
			}
		}
	}
}
