package modicum.lib;

import modicum.lib.ColorTools.Rgba;

class CssTools {
	
	public static function cssVendorize(s:String) {
		return '-moz-$s;-webkit-$s;-ms-$s;$s';
	}

	public static function cssVendorize2(s:String, prefix:String) {
		return '$prefix-moz-$s;$prefix-webkit-$s;$prefix-ms-$s;$prefix$s';
	}

	public static function cssMakeSelectable() {
		return '-webkit-touch-callout:text;'
			+ '-webkit-user-select:text;'
			+ '-khtml-user-select:text;'
			+ '-moz-user-select:text;'
			+ '-ms-user-select:text;'
			+ 'user-select:text;';
	}

	public static function cssMakeNonSelectable() {
		return '-webkit-touch-callout:none;'
			+ '-webkit-user-select:none;'
			+ '-khtml-user-select:none;'
			+ '-moz-user-select:none;'
			+ '-ms-user-select:none;'
			+ 'user-select:none;';
	}

	public static inline function cssFullRgb(s:String) {
		return ColorTools.fullRgb(s);
	}

	public static inline function cssColor2Components(s:String): Rgba {
		return ColorTools.color2Components(s);
	}

	public static inline function cssComponents2Color(rgba:Rgba): String {
		return ColorTools.components2Color(rgba);
	}

	public static inline function cssColorOffset(col:String,
												offset:Dynamic,
												saturation=1.0,
												?alpha:Float): String {
		return ColorTools.colorOffset(col, offset, saturation, alpha);
	}

	public static inline function cssCounterColor(col:String,
												col1='black',
												col2='white',
												threshold=176): String {
		return ColorTools.counterColor(col, col1, col2, threshold);
	}

	public static inline function cssColorMix(col1:String,
											col2:String,
											ratio:Float): String {
		return ColorTools.mix(col1, col2, ratio);
	}

}
